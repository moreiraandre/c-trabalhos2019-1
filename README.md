# Trabalhos `C` 2019/1

## Clonar repositório
```bash
git clone https://gitlab.com/moreiraandre/c-trabalhos2019-1.git
```

## Descrição das pastas
*  **T1**: Pilha, fila, lista: estático
*  **T2**: Pilha, fila, lista: dinâmico
*  **T3**: Programa principal com nome p/ testes de T1 e T2
*  **T4**: Fila Circular

## Executar
Dentro de cada pasta execute `make && ./saida`.
