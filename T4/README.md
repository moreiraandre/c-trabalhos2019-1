# Fila Circular em _C_

# Clonar repositório
```
git clone https://gitlab.com/moreiraandre/c-fila-circular.git
```

# Compilar
```
cd c-fila-circular
make
```

# Executar
```
./principal
```
