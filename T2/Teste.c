#include <stdio.h>
#include "Teste.h"

int testeInserir(PDesc desc, int qtd) {
    int x;
    
    for (x = 0; x < qtd; x++) {
        Dado d;
        d.chave = x;
        inserirFim(desc, &d);
        printf("*INSERIDO %i\n", d.chave);
    }
    
    return 0;
}

int testeRemover(PDesc desc, int qtd) {
    int x;
    
    for (x = 0; x < qtd; x++) {
        removerChave(desc, x);
        printf("*REMOVIDO %i\n", x);
    }
    
    return 0;
}
