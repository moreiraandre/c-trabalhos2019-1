#include <stdlib.h>
#include <stdio.h>
#include "Descritor.h"

PDesc criar() {
    PDesc p = (PDesc) malloc(sizeof(Descritor));
    if (p != NULL) {
        p->topo = NULL;
        p->fim = NULL;
        p->length = 0;
    }
    
    return p;
}

int inserirFim(PDesc l, PDado d) {
    PNo p = (PNo) malloc(sizeof(No));
    if (p == NULL)
        return -1; // ERRO DE MEMÓRIA
    p->d = d;
    p->prox = NULL;
    if (l->length == 0) { // INSERE PRIMEIRO ELEMENTO
        l->topo = p;
        l->fim = p;
        l->length = 1;
    } else {
        l->fim->prox = p;
        l->fim = p;
        l->length++;
    }
    
    return 0;
}

int inserirInicio(PDesc l, PDado d) {
    PNo p = (PNo) malloc(sizeof(No));
    if (p == NULL)
        return -1;
    p->d = d;
    p->prox = NULL;
    if (l->length == 0) { // INSERE PRIMEIRO ELEMENTO
        l->topo = p;
        l->fim = p;
    } else {
        p->prox = l->topo;
        l->topo = p;
    }
    l->length++;
    
    return 0;
}

int inserirPos(PDesc l, PDado dado, int pos) {
    // PNo p = criarNo(dado);
    PNo p = (PNo) malloc(sizeof(No));
    if (p == NULL)
        return -1;
    
    if (pos > l->length) {
        inserirFim(l, dado);
        return 0;
    }
    
    PNo aux;
    aux = l->topo;
    int i;
    for (i = 0; i < pos - 1; i++)
        aux = aux->prox;
    p->prox = aux->prox;
    aux->prox = p;
    
    return 0;
}

PNo buscaEndereco(PDesc l, PDado dado) {
    PNo aux;
    aux = l->topo;
    while (aux != NULL && aux->d != dado)
        aux = aux->prox;
    
    return aux;
}

int buscaChave(PDesc l, int ch) {
    PNo aux;
    aux = l->topo;
    while (aux != NULL) {
        if (aux->d->chave == ch)
            // PROCESSA REGISTRO
            printf("ACHOU!\n");
            
        aux = aux->prox;
    }
    
    return 0;
}

int removerChave(PDesc l, int ch) {
    if (l->length == 0) return 0;
    PNo aux;
    if (l->topo->d->chave == ch) { // REMOVE TOPO
        aux = l->topo;
        if (l->topo == l->fim) {
            l->topo = NULL;
            l->fim = NULL;
            l->length = 0;
        }
        //free(aux); // QND UTILIZADO APRESENTA ERRO DE SEGMENTAÇÃO
        return 0;
    }
    
    PNo ant;
    ant = l->topo;
    PNo atual;
    atual = ant->prox;
    while (atual != NULL) {
        if (atual->d->chave == ch) {
            aux = atual;
            ant->prox = atual->prox;
            atual = atual->prox;
            free(aux);
        } else {
            ant = atual;
            atual = atual->prox;
        }
    }
    
    return 0;
}

