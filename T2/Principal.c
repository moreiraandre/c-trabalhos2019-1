#include <stdio.h>
#include "Teste.h"

PDesc m;

void teste(){
    int qtd;
    m = criar();
    printf("-->TESTE INSERIR\n");
    printf("INFORME A QUANTIDADE DE VEZES PARA INSERIR: ");
    scanf("%i", &qtd);
    testeInserir(m, qtd);
    printf("-->TESTE REMOVER\n");
    testeRemover(m, qtd);
}

int main () {
    teste();
    
    return 0;
}
