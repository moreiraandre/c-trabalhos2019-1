#include <stdio.h>
#include "../../lists/headers/ListaInt.h"
#include "../../tests/headers/testeListaInt.h"

// Teste de insercao
void testeInserirLista(PDado lista){
   int i=0;
	printf("Inicio do teste de insercao.\n");
	printf("Serão INSERIDOS inteiros até que a lista fique cheia\n");
	while (pushLista(lista, i, direcaoInserirLista) != -1)
		printf("Inseriu: %d\n", i++);
	printf("Lista cheia. \n Fim do teste de insercao.\n========================\n\n");
}

// Teste de remocao
void testeRemoverLista(PDado lista){
   int valor;
	printf("Inicio do teste de remocao.\n");
	printf("Serão REMOVIDOS inteiros até que a lista fique vazia\n");
	while (popLista(lista, &valor, !direcaoInserirLista) != -1)
		printf("Removeu: %d\n", valor);
	printf("Lista vazia. \n Fim do teste de remocao.\n========================\n\n");
}

void testeLeituraArquivoLista(PDado lista){
  readLista("inteiro.txt", lista);
}



