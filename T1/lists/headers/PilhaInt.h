#ifndef PilhaInt_xxx_1234

#include "Dado.h"

//Insere um elemento na pilha. 
//Retorna -1 caso a pilha esteja cheia. 
int pushPilha(PDado, int);

//Retira um elemento na pilha 
//Retorna -1 caso a pilha esteja vazia. 
int popPilha(PDado, int*);

void readPilha(char arquivo[], PDado);

#define PilhaInt_xxx_1234

#endif

