#ifndef Dado_xxx_1234

#define MAX 10
typedef struct {
	int dado[MAX];
	int topo;
}Dado;

typedef Dado *PDado;

//Atribui valores iniciais para a variável criada
void criar(PDado);
int isFull(PDado);
int isEmpty(PDado);
#define Dado_xxx_1234

#endif

