#ifndef FilaInt_xxx_1234

#include "Dado.h"

//Insere um elemento na Fila. 
//Retorna -1 caso a Fila esteja cheia. 
int pushFila(PDado, int);

//Retira um elemento na Fila 
//Retorna -1 caso a Fila esteja vazia. 
int popFila(PDado, int*);

void readFila(char arquivo[], PDado);

#define FilaInt_xxx_1234

#endif

