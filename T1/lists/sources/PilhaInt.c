#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "../headers/PilhaInt.h"

//Insere um elemento na pilha. 
//Retorna -1 caso a pilha esteja cheia. 
int pushPilha(PDado pilha, int valor){
  if (isFull(pilha))
    return -1;
  pilha->topo++;
  pilha->dado[pilha->topo]=valor;
  return 0;
}

//Retira um elemento na pilha 
//Retorna -1 caso a pilha esteja vazia. 
int popPilha(PDado pilha, int* valor){
  if (isEmpty(pilha))
    return -1;
  *valor = pilha->dado[pilha->topo];
  pilha->topo--;
  return 0;
}

// Lê um conjunto de números inteiros do arquivo "inteiro.txt",
// cria uma pilha e os coloca na pilha.
void readPilha(char arquivo[], PDado p){
  criar(p);
  char numStr[10];
  int valor;
  FILE *arq;
  arq = fopen(arquivo, "r");

  if(arq == NULL)
    printf("Erro, nao foi possivel abrir o arquivo\n");
  else{
//    while (fscanf(arq, "%s", numStr) != EOF){
    while (fscanf(arq, "%d", &valor) != EOF){
     // valor = atoi(numStr);
      printf("Valor lido: %d   ", valor);
      if (isFull(p)){
        printf("Pilha cheia ... \n");
        break;
      }
      if (pushPilha(p, valor) != -1)
        printf("Inserido %d\n", valor);
    }
  }
  fclose(arq);
}
 
