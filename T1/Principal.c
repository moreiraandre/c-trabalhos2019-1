#include <stdio.h>
#include "lists/headers/PilhaInt.h"
#include "lists/headers/FilaInt.h"
#include "lists/headers/ListaInt.h"
#include "tests/headers/testePilhaInt.h"
#include "tests/headers/testeFilaInt.h"
#include "tests/headers/testeListaInt.h"

// *** LISTAS DE TESTE ***
Dado pilha;
Dado fila;
Dado lista;

// *** FUNÇÕES DE TESTE ***
void testePilha(){
	criar(&pilha);
	printf("INICIOU A PILHA!\n");
	testeInserirPilha(&pilha);
	testeRemoverPilha(&pilha);
	testeLeituraArquivoPilha(&pilha);
	testeRemoverPilha(&pilha);
	printf("FIM.\n");
}

void testeFila(){
	criar(&fila);
	printf("INICIOU A FILA!\n");
	testeInserirFila(&fila);
	testeRemoverFila(&fila);
	testeLeituraArquivoFila(&fila);
	testeRemoverFila(&fila);
	printf("FIM.\n");
}

void testeLista(){
    printf("INFORME A DIRECAO PARA INSERÇÃO (RETIRADA SERA O OPOSTO) DA LISTA (0=ESQUERDA 1=DIREITA): ");
    scanf("%i", &direcaoInserirLista);
	criar(&lista);
	printf("INICIOU A LISTA!\n");
	testeInserirLista(&lista);
	testeRemoverLista(&lista);
	testeLeituraArquivoLista(&lista);
	testeRemoverLista(&lista);
	printf("FIM.\n");
}

int main(int argc, char* argv[]){
    int sair; // RECEBE AS OPÇÕES E A RESPOSTA DE SAIR
    
    do {
        printf("\n*** MENU ***");
        printf("\n============\n");
        printf("1 - PILHA\n");
        printf("2 - FILA\n");
        printf("3 - LISTA\n");
        printf("------------\n");
        printf("DIGITE A OPÇÃO DESEJADA: ");
        scanf("%i", &sair);
        switch (sair) {
            case 1: testePilha(); break;
            case 2: testeFila(); break;
            case 3: testeLista(); break;
        }
        printf("DESEJA SAIR? (1=SIM, 0=NAO): ");
        scanf("%i", &sair);
        
    } while(sair == 0);
    
    return 0;
}
