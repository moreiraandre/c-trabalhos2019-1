#include <stdio.h>
#include <stdlib.h>

int main() {
    int sair; // RECEBE AS OPÇÕES E A RESPOSTA DE SAIR
    
    do {
        system("clear");
        printf("\n*** MENU ***");
        printf("\n============\n");
        printf("1 - LISTAS ESTATICAS\n");
        printf("2 - LISTA DINAMICA\n");
        printf("------------\n");
        printf("DIGITE A OPÇÃO DESEJADA: ");
        scanf("%i", &sair);
        switch (sair) {
            case 1: system("cd ./../T1 && ./saida"); break;
            case 2: system("cd ./../T2 && ./saida"); break;
        }
        printf("DESEJA ENCERRAR? (1=SIM, 0=NAO): ");
        scanf("%i", &sair);
        
    } while(sair == 0);
    
    return 0;
}
